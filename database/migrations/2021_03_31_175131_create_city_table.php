<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city', function (Blueprint $table) {
            $table->id();
            $table->foreignId('province_id')->constrained('provinced')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('jumlah_kota')->nullable();
            $table->integer('jumlah_kabupaten')->nullable();
            $table->integer('jumlah_total')->nullable();
            $table->timestamps();
        });
        // Schema::table('city', function (Blueprint $table) {
        //     $table->foreignId('province_id')->references('id')->on('province')->onDelete('cascade')->onUpdate('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city');
    }
}
