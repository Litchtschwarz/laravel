@extends('layout/main')

@section('title', 'Jumlah Kota & Kabupaten')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10"></div>
            <h1 class="mt-3">Jumlah Kota & Kabupaten Tiap Provinsi</h1>

            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <table>
                <table class="table">
                    <thead table class="table-dark">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Provinsi</th>
                            <th scope="col">Jumlah Kota</th>
                            <th scope="col">Jumlah Kabupaten</th>
                            <th scope="col">Jumlah Total</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($city as $cty)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $cty->provinsi->name_province }}</td>
                                <td>{{ $cty->jumlah_kota }}</td>
                                <td>{{ $cty->jumlah_kabupaten }}</td>
                                <td>{{ $cty->jumlah_total }}</td>
                                <td>
                                    <a href="" class="badge bg-success">Edit</a>
                                    <a href="" class="badge bg-danger">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </table>

            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                <a href="{{ url('/city/create') }}" class="btn btn-primary">Tambah Data</a>
            </div>

        </div>
    </div>
    </div>
@endsection
