@extends('layout/main')

@section('title', 'Form Tambah Data Jumlah Kota & Kabupaten')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10"></div>
            <h1 class="mt-3">Jumlah Kota & Kabupaten</h1>

            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <form method="post" action="{{ url('city') }}">
                @csrf
                <div class="input-group mb-3">
                    <label class="input-group-text" for="province_id">Nama Provinsi</label>
                    <select name="province_id" class="form-select @error('province_id') is-invalid @enderror"
                        id="province_id">
                        <option selected>Pilih Provinsi</option>
                        @foreach ($provinsi as $province_id)
                            <option value="{{ $province_id->id }}">{{ $province_id->name_province }}</option>
                        @endforeach
                    </select>
                    @error('province_id')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="jumlah_kota" class="form-label">Jumlah Kota</label>
                    <input name="jumlah_kota" type="text" class="form-control @error('jumlah_kota') is-invalid @enderror"
                        id="jumlah_kota" placeholder="Masukkan Jumlah Kota">
                </div>

                <div class="mb-3">
                    <label for="jumlah_kabupaten" class="form-label">Jumlah Kabupaten</label>
                    <input name="jumlah_kabupaten" type="text"
                        class="form-control @error('jumlah_kabupaten') is-invalid @enderror" id="jumlah_kabupaten"
                        placeholder="Masukkan Jumlah Kabupaten">
                </div>

                <div class="mb-3">
                    <label for="jumlah_total" class="form-label">Jumlah Total</label>
                    <input name="jumlah_total" type="text" class="form-control" id="jumlah_total"
                        placeholder="Masukkan Jumlah total">
                </div>

                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                    <button type="submit" class="btn btn-primary me-md-2" type="button">Submit</button>
                    <a href="{{url('city/index')}}" class="btn btn-secondary me-md-2">Kembali</a>
                </div>
            </form>
        </div>
    </div>
    </div>
@endsection
