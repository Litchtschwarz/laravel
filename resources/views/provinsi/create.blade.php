@extends('layout/main')

@section('title', 'Form Tambah Data Jumlah Kota & Kabupaten')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10"></div>
            <h1 class="mt-3">Nama Provinsi</h1>

            <form method="post" action="{{ url('provinsi') }}">
                @csrf
                <div class="mb-3">
                    <label for="name_province" class="form-label">Nama Provinsi</label>
                    <input name="name_province" type="text" class="form-control @error('name_province') is-invalid @enderror"
                        id="name_province" placeholder="Masukkan Nama Provinsi">
                </div>
                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                        <button type="submit" class="btn btn-primary me-md-2" type="button">Tambah Data</button>
                        <a href="{{url('provinsi/index')}}" class="btn btn-secondary me-md-2">Kembali</a>
                </div>
            </form>
        </div>
    </div>
    </div>
@endsection
