@extends('layout/main')

@section('title', 'Nama Provinsi')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10"></div>
            <h1 class="mt-10">Nama Provinsi</h1>

            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <table class="table">
                <thead table class="table-dark">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($provinsi as $pvc)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $pvc->name_province }}</td>
                            <td>
                                <a href="{{ url('provinsi/'.$pvc->id.'/edit') }}" class="badge bg-success">Edit</a>
                                <a href="" class="badge bg-danger">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                <a href="{{ url('/provinsi/create') }}" class="btn btn-primary">Tambah Data</a>
            </div>
        </div>
    </div>
    </div>
@endsection
