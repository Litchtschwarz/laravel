@extends('layout/main')

@section('title','Jumlah Penduduk Indonesia')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-10"></div>
        <h1 class="mt-3">Jumlah Penduduk Indonesia</h1>

        <table>
            <table class="table">
                <thead table class="table-dark">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama </th>
                        <th scope="col">Tahun</th>
                        <th scope="col">Laki-Laki</th>
                        <th scope="col">Perempuan</th>
                        <th scope="col">Jumlah Total</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($population as $poput)
                    <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{ $poput->nama_provinsi }}</td>
                    <td>{{ $poput->tahun}}</td>
                    <td>{{ $poput->laki_laki}}</td>
                    <td>{{ $poput->perempuan}}</td>
                    <td>{{ $poput->jumlah_penduduk}}</td>
                    <td></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </table>
    </div>
</div>
</div>
@endsection