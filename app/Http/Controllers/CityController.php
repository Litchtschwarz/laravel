<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Provinsi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class CityController extends Controller
{
    public function city()
    {
        // $city = DB::table('city')->get();
        $city = City::all();
        return view('/city/index', compact('city'));
    }
    
    public function create()
    {
        $provinsi = Provinsi::all();
        return view('/city/create', compact('provinsi'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'province_id' => 'required'
        ],[
            'province_id.required' => 'Pilih Nama Provinsi'
        ]);
        // return $request;
        $city = new City;
        $city->province_id = $request->province_id;
        $city->jumlah_kota = $request->jumlah_kota;
        $city->jumlah_kabupaten = $request->jumlah_kabupaten;
        $city->jumlah_total = $request->jumlah_total;
        $city->save();

        return redirect('/city/index')->with('status', 'Data Berhasil Ditambahkan');
    }

    public function edit(City $city)
    {
        
    }
}

