<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Provinsi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class ProvinsiContorller extends Controller
{
    public function provinsi()
    {
        // $province = DB::table('provinced')->get();
        $provinsi = Provinsi::all();
        return view('/provinsi/index', compact('provinsi'));
    }
    public function create()
    {
        return view('provinsi/create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name_province' => 'required'
        ], [
            'name_province.required' => 'Masukkan Nama Provinsi'
        ]);
        // return $request;
        $provinsi = new Provinsi;
        $provinsi->name_province = $request->name_province;
        $provinsi->save();

        return redirect('/provinsi/index')->with('status', 'Data Berhasil Ditambahkan');
    }

    // public function show(Provinsi $show)
    // {
    //     return view('provinsi/show', compact('provinsi'));
    // }

    public function edit(Provinsi $provinsi)
    {
        return view('provinsi/edit', compact('provinsi'));
    }

    public function update(Request $request, Provinsi $provinsi)
    {
        $request->validate([
            'name_province' => 'required'
        ]);

        $provinsi->update($request->all());

        return redirect('/provinsi/index')->with('status', 'Data Berhasil Dirubah');
    }
}
