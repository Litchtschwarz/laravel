<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use phpDocumentor\Reflection\Types\This;
use App\Models\Provinsi;

class City extends Model
{
    protected $table = 'city';

    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'province_id');
    }
    
}
