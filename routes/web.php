<?php

use App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\ProvinceController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\ProvinsiContorller;
use App\Models\Provinsi;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

// Route::get('/about', function () {
//     return view('about');
// });

// Route::get('/datafor', function () {
//     return view('datafor');
// });

// Route::get('/dataforeach', function () {
//     return view('dataforeach');
// });


// // Route::get('/', 'App\Http\Controllers\PagesController@home');
// // Route::get('/login', 'App\Http\Controllers\Auth\LoginController@login');
// // Route::get('/about', 'App\Http\Controllers\PagesController@about');

// // //Data_Provinsi
// // Route::get('/provinsi', 'App\Http\Controllers\ProvinsiController@index');
// // Route::get('/provinsi/create', 'App\Http\Controllers\ProvinsiController@create');
// // Route::post('/provinsi', 'App\Http\Controllers\ProvinsiController@post');


// // //Data_Kota
// // Route::get('/city', 'App\Http\Controllers\CityController@city')->name('city.index');
// // Route::post('/city', 'App\Http\Controllers\CityController@store')->name('city.post');
// // Route::get('/city/create', 'App\Http\Controllers\CityController@create')->name('city.create');
// // Route::get('/city/{city}', 'App\Http\Controllers\CityController@show')->name('city.show');

// //Data_Penduduk
// Route::get('/penduduk', 'App\Http\Controllers\PendudukController@penduduk');


// Auth::routes();
// Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// ------------------------ //
Route::get('/', [PagesController::class, 'home']);
Route::get('/about', [PagesController::class, 'about']);

// Provinsi
Route::get('/provinsi/index', [ProvinsiContorller::class, 'provinsi']);
Route::get('/provinsi/create', [ProvinsiContorller::class, 'create']);
Route::post('/provinsi', [ProvinsiContorller::class, 'store']);
Route::get('/provinsi/{$id}/edit', [ProvinsiContorller::class, 'edit']);
Route::patch('provinsi/{$id}', [ProvinsiContorller::class, 'update']);


//City
Route::get('/city/index', [CityController::class, 'city']);
Route::post('/city', [CityController::class, 'store']);
Route::get('/city/create', [CityController::class, 'create']);

//Auth
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
